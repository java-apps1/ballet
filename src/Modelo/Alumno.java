/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;

/**
 *
 * @author gpr
 */
public class Alumno {
 SimpleDateFormat Formato = new SimpleDateFormat("yyyy-MM-dd");

    private int id_alumno;
    private String nombre_alumno;
    private String apellidoP_alumno;
    private String apellidoM_alumno;
    private String telefono_alumno;
    private String direccion_alumno;
    private String fechaNac_alumno;
    private String alergias_alumno;
    private String tutor_alumno;
    private String fecha_inscripcion_alumno;

    public Alumno() {
    }

    public int getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(int id_alumno) {
        this.id_alumno = id_alumno;
    }

    public String getNombre_alumno() {
        return nombre_alumno;
    }

    public void setNombre_alumno(String nombre_alumno) {
        this.nombre_alumno = nombre_alumno;
    }

    public String getApellidoP_alumno() {
        return apellidoP_alumno;
    }

    public void setApellidoP_alumno(String apellidoP_alumno) {
        this.apellidoP_alumno = apellidoP_alumno;
    }

    public String getApellidoM_alumno() {
        return apellidoM_alumno;
    }

    public void setApellidoM_alumno(String apellidoM_alumno) {
        this.apellidoM_alumno = apellidoM_alumno;
    }

    public String getTelefono_alumno() {
        return telefono_alumno;
    }

    public void setTelefono_alumno(String telefono_alumno) {
        this.telefono_alumno = telefono_alumno;
    }

    public String getDireccion_alumno() {
        return direccion_alumno;
    }

    public void setDireccion_alumno(String direccion_alumno) {
        this.direccion_alumno = direccion_alumno;
    }

    public String getFechaNac_alumno() {
        return fechaNac_alumno;
    }

    public void setFechaNac_alumno(String fechaNac_alumno) {
        this.fechaNac_alumno = fechaNac_alumno;
    }

    public String getAlergias_alumno() {
        return alergias_alumno;
    }

    public void setAlergias_alumno(String alergias_alumno) {
        this.alergias_alumno = alergias_alumno;
    }

    public String getTutor_alumno() {
        return tutor_alumno;
    }

    public void setTutor_alumno(String tutor_alumno) {
        this.tutor_alumno = tutor_alumno;
    }

    public String getFecha_inscripcion_alumno() {
        return fecha_inscripcion_alumno;
    }

    public void setFecha_inscripcion_alumno(String fecha_inscripcion_alumno) {
        this.fecha_inscripcion_alumno = fecha_inscripcion_alumno;
    }

    public String getFecha(JDateChooser jd) {
        if (jd.getDate() != null) {
            return Formato.format(jd.getDate());
        } else {
            return null;
        }
    }

}
