/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author gpr
 */
public class Empleado {

    private int id_empleado;
    private String nombre_empleado;
    private String apellidoP_empleado;
    private String apellidoM_empleado;
    private String direccion_empleado;
    private String telefono_empleado;
    private double salario_base_empleado;
    private int id_rol;
    private String rol;

    public Empleado() {
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public String getNombre_empleado() {
        return nombre_empleado;
    }

    public void setNombre_empleado(String nombre_empleado) {
        this.nombre_empleado = nombre_empleado;
    }

    public String getApellidoP_empleado() {
        return apellidoP_empleado;
    }

    public void setApellidoP_empleado(String apellidoP_empleado) {
        this.apellidoP_empleado = apellidoP_empleado;
    }

    public String getApellidoM_empleado() {
        return apellidoM_empleado;
    }

    public void setApellidoM_empleado(String apellidoM_empleado) {
        this.apellidoM_empleado = apellidoM_empleado;
    }

    public String getDireccion_empleado() {
        return direccion_empleado;
    }

    public void setDireccion_empleado(String direccion_empleado) {
        this.direccion_empleado = direccion_empleado;
    }

    public String getTelefono_empleado() {
        return telefono_empleado;
    }

    public void setTelefono_empleado(String telefono_empleado) {
        this.telefono_empleado = telefono_empleado;
    }

    public double getSalario_base_empleado() {
        return salario_base_empleado;
    }

    public void setSalario_base_empleado(double salario_base_empleado) {
        this.salario_base_empleado = salario_base_empleado;
    }

    public int getId_rol() {

        //  1 Directora
        //  2 Recepcionista
        //  3 Maestro 
        //  4 Intendencia
        if (rol.equals("Directora")) {
            return 1;
        } else if (rol.equals("Recepcionista")) {
            return 2;
        } else if (rol.equals("Maestro")) {
            return 3;
        } else {
            return 4;
        }
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public String getRol() {

        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
