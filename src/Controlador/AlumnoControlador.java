/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Alumno;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author gpr
 */
public class AlumnoControlador {

    public boolean actualizar(Alumno alu) {
        boolean isUpdate = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "UPDATE alumno SET"
                + " nombre_alumno='" + alu.getNombre_alumno() + "',"
                + " apellidoP_alumno='" + alu.getApellidoP_alumno() + "',"
                + " apellidoM_alumno='" + alu.getApellidoM_alumno() + "',"
                + " telefono_alumno='" + alu.getTelefono_alumno() + "',"
                + " direccion_alumno='" + alu.getDireccion_alumno() + "',"
                + " fechaNac_alumno='" + alu.getFechaNac_alumno() + "',"
                + " alergias_alumno='" + alu.getAlergias_alumno() + "',"
                + " tutor_alumno='" + alu.getTutor_alumno() + "',"
                + " fecha_inscripcion_alumno='" + alu.getFecha_inscripcion_alumno() + "'"
                + " WHERE id_alumno='" + alu.getId_alumno() + "'";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            isUpdate = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isUpdate;

    }

    public boolean insertar(Alumno alumno) {
        boolean isInsert = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "INSERT INTO alumno ("
                + " nombre_alumno,"
                + " apellidoP_alumno,"
                + " apellidoM_alumno,"
                + " telefono_alumno,"
                + " direccion_alumno,"
                + " fechaNac_alumno,"
                + " alergias_alumno,"
                + " tutor_alumno,"
                + " fecha_inscripcion_alumno"
                + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, alumno.getNombre_alumno());
            ps.setString(2, alumno.getApellidoP_alumno());
            ps.setString(3, alumno.getApellidoM_alumno());
            ps.setString(4, alumno.getTelefono_alumno());
            ps.setString(5, alumno.getDireccion_alumno());
            ps.setString(6, alumno.getFechaNac_alumno());
            ps.setString(7, alumno.getAlergias_alumno());
            ps.setString(8, alumno.getTutor_alumno());
            ps.setString(9, alumno.getFecha_inscripcion_alumno());

            int rowsInserted = ps.executeUpdate();
            if (rowsInserted > 0) {
                isInsert = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isInsert;

    }

//       private int id_alumno;
//    private String nombre_alumno;
//    private String apellidoP_alumno;
//    private String apellidoM_alumno;
//    private String telefono_alumno;
//    private String direccion_alumno;
//    private String fechaNac_alumno;
//    private String alergias_alumno;
//    private String tutor_alumno;
//    private String fecha_inscripcion_alumno;
    public ArrayList<Alumno> Listar_alumnos() {
        ArrayList<Alumno> list = new ArrayList<>();
        Connection conn = Modelo.conexionMysql.getConnection();

        String sql = "SELECT * FROM alumno;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Alumno alumno = new Alumno();
                alumno.setId_alumno(rs.getInt(1));
                alumno.setNombre_alumno(rs.getString(2));
                alumno.setApellidoP_alumno(rs.getString(3));
                alumno.setApellidoM_alumno(rs.getString(4));
                alumno.setTelefono_alumno(rs.getString(5));
                alumno.setDireccion_alumno(rs.getString(6));
                alumno.setFechaNac_alumno(rs.getString(7));
                alumno.setAlergias_alumno(rs.getString(8));
                alumno.setTutor_alumno(rs.getString(9));
                alumno.setFecha_inscripcion_alumno(rs.getString(10));
                list.add(alumno);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception ex) {
            }
        }
        return list;
    }
    
      public boolean eliminar(int id_alumno) {
        boolean isDelete = false;
        java.sql.Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "DELETE FROM alumno where id_alumno='" + id_alumno + "'";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            isDelete = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isDelete;

    }

}
