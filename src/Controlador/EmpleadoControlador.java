package Controlador;

import Modelo.Empleado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gpr
 */
public class EmpleadoControlador {


    public boolean insertar(Empleado emp) {
        boolean isInsert = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "INSERT INTO empleado ("
                + " nombre_empleado,"
                + " apellidoP_empleado,"
                + " apellidoM_empleado,"
                + " direccion_empleado,"
                + " telefono_empleado,"
                + " salario_base_empleado,"
                + " id_rol"
                + ") VALUES (?, ?, ?, ?,?,?,?)";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, emp.getNombre_empleado());
            ps.setString(2, emp.getApellidoP_empleado());
            ps.setString(3, emp.getApellidoM_empleado());
            ps.setString(4, emp.getDireccion_empleado());
            ps.setString(5, emp.getTelefono_empleado());
            ps.setDouble(6, emp.getSalario_base_empleado());
            ps.setInt(7, emp.getId_rol());

            int rowsInserted = ps.executeUpdate();
            if (rowsInserted > 0) {
                isInsert = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isInsert;

    }

    public boolean actualizar(Empleado emp) {
        boolean isUpdate = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "UPDATE empleado SET"
                + " nombre_empleado='" + emp.getNombre_empleado() + "',"
                + " apellidoP_empleado='" + emp.getApellidoP_empleado() + "',"
                + " apellidoM_empleado='" + emp.getApellidoM_empleado() + "',"
                + " direccion_empleado='" + emp.getDireccion_empleado() + "',"
                + " telefono_empleado='" + emp.getTelefono_empleado() + "',"
                + " salario_base_empleado='" + emp.getSalario_base_empleado() + "',"
                + " id_rol='" + emp.getId_rol() + "'"
                + " WHERE id_empleado='" + emp.getId_empleado() + "'";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            isUpdate = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isUpdate;

    }

    public boolean eliminar(int id_empleado) {
        boolean isDelete = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "DELETE FROM empleado where id_empleado='" + id_empleado + "'";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            isDelete = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isDelete;

    }

    public ArrayList<Empleado> Listar_empleados() {
        ArrayList<Empleado> list = new ArrayList<>();
        Connection conn = Modelo.conexionMysql.getConnection();

        String sql = "SELECT * FROM empleado join rol on rol.id_rol=empleado.id_rol order by id_empleado;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Empleado empleado = new Empleado();

                empleado.setId_empleado(rs.getInt(1));
                empleado.setNombre_empleado(rs.getString(2));
                empleado.setApellidoP_empleado(rs.getString(3));
                empleado.setApellidoM_empleado(rs.getString(4));
                empleado.setDireccion_empleado(rs.getString(5));
                empleado.setTelefono_empleado(rs.getString(6));
                empleado.setSalario_base_empleado(rs.getDouble(7));
                empleado.setId_rol(rs.getInt(8));
                empleado.setRol(rs.getString(10));
                list.add(empleado);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception ex) {
            }
        }
        return list;
    }

    

   

}
