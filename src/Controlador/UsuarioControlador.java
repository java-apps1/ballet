/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author gpr
 */
public class UsuarioControlador {
    
    
     public boolean insertar(Usuario usuario) {
        boolean isInsert = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "INSERT INTO usuario ("
                + " nombre_usuario,"
                + " contrasena_usuario,"
                + " id_empleado"
                + ") VALUES (?, ?, ?)";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, usuario.getNombre_usuario());
            ps.setString(2, usuario.getContasena_usuario());
            ps.setInt(3, usuario.getId_empleado());
           

            int rowsInserted = ps.executeUpdate();
            if (rowsInserted > 0) {
                isInsert = true;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isInsert;

    }

    public boolean actualizar(Usuario usuario) {
        boolean isUpdate = false;
        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "UPDATE usuario SET"
                + " nombre_usuario='" + usuario.getNombre_usuario() + "',"
                + " contrasena_usuario='" + usuario.getContasena_usuario() + "'"
                + " WHERE id_empleado='" + usuario.getId_empleado() + "'";

        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
            isUpdate = true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (Exception ex) {
            }
        }
        return isUpdate;

    }

    public Usuario getUsuario(int id_empleado) {

        Connection conn = Modelo.conexionMysql.getConnection();
        String sql = "SELECT * FROM usuario where id_empleado='" + id_empleado + "';";
        ResultSet rs = null;
        PreparedStatement ps = null;

        Usuario usuario = new Usuario();
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                usuario.setId_usuario(rs.getInt(1));
                usuario.setNombre_usuario(rs.getString(2));
                usuario.setContasena_usuario(rs.getString(3));
                usuario.setId_empleado(rs.getInt(4));
                usuario.setIsAuth(true);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
            } catch (Exception ex) {
            }
        }
        return usuario;
    }
}
