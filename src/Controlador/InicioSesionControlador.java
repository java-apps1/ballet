/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author gpr
 */
public class InicioSesionControlador {

    public Usuario  iniciarSesion(String nombre_usuario, String contrasena_usuario) throws SQLException {

        Connection conn = Modelo.conexionMysql.getConnection();
        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery("select nombre_usuario,contrasena_usuario,nombre_rol,empleado.id_rol from usuario\n"
                + "join empleado on empleado.id_empleado=usuario.id_empleado\n"
                + "join rol on rol.id_rol=empleado.id_rol");

       Usuario usuario = new Usuario();

        while (rs.next()) {
            
            if( nombre_usuario.equals(rs.getString(1)) && contrasena_usuario.equals(rs.getString(2)) ){
               usuario.setNombre_usuario(rs.getString(1));
               usuario.setRol(rs.getString(3));
               usuario.setIsAuth(true);
               break;
            }
        }
        return usuario;
    }

}
